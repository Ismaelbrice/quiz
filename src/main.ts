const questionsrep = [
  {
    questions : "Qui est le meilleur marqueur de l'histoire de la nba?" ,
    reponses : [
      {Text : "Michaël Jordan" , correct : false} ,
      {Text : "Lebron James" , correct : true} ,
      {Text : "David Gilmour" , correct : false} ,
      {Text : "Kobe Bryant" , correct : false} ,
    ]
  } ,
  {
    questions : "Quelle équipe a été championne de la saison 2022-2023 ?" ,
    reponses : [
      {Text : "Warriors st Fansisco" , correct : false} ,
      {Text : "FC t'inquiète-basketball" , correct : false} ,
      {Text : "Denver Nugget" , correct : true} ,
      {Text : "Monique gang" , correct : false} ,
    ]
  } ,
  {
    questions : "Qui est le meilleur tireur à 3 points de l'histoire de la nba?" ,
    reponses : [
      {Text : "Stephen Curry" , correct : true} ,
      {Text : "Mickey Mouse" , correct : false} ,
      {Text : "Ismaël Samaké" , correct : false} ,
      {Text : "Magic Johnson" , correct : false} ,
    ]
  } ,
  {
    questions : "Qu'est ce qui est petit carré et jaune?" ,
    reponses : [
      {Text : "lil Biscuit" , correct : false} ,
      {Text : "Un grand Cercle rouge" , correct : false} ,
      {Text : "Romane Dieu" , correct : false} ,
      {Text : "lil carré jaune" , correct : true} ,
    ]
  } ,
  {
    questions : "Qui est le français le plus titré de la NBA?" ,
    reponses : [
      {Text : "Michaël Piétrus" , correct : false} ,
      {Text : "Victor Wemby" , correct : false} ,
      {Text : "Tony Parker" , correct : true} ,
      {Text : "Jean pierre Pernot" , correct : false} ,
    ]
  }
];

let mesquestions = document.querySelector<HTMLElement> ("#question") ;
let repbutton = document.querySelector<HTMLButtonElement> ("#rep-button1") ;
let btnSuivant = document.querySelector<HTMLButtonElement> ("#btn-2") ;

let indexquestion = 0 ;
let score = 0 ;

function lancerjeux () {
  indexquestion = 0 ;
  score = 0 ;
  btnSuivant.innerHTML = "Suivant"; 
  MontrerQuestions() ;
}

function MontrerQuestions() {
  reset() ;
  let listeQuestions = questionsrep [indexquestion] ;
  let questionsNum = indexquestion + 1 ;
  mesquestions.innerHTML = questionsNum + "." + listeQuestions.questions ;

  listeQuestions.reponses.forEach( rep => {
    let button = document.createElement ("button") ;
    button.innerHTML = rep.Text ;
    button.classList.add ("bnt") ;
    repbutton.appendChild(button) ;
    if (rep.correct ) {
      button.dataset.correct = rep.correct+'' ;
    }
    button.addEventListener ( "click" , selections ) ;
  } 
  );
}

function reset () {
  btnSuivant.style.display = "none" ;
  while (repbutton.firstChild) {
    repbutton.removeChild ( repbutton.firstChild) ;
  }
}

function selections (e:any) {
const selectbtn = e.target ;
const truerep = selectbtn.dataset.correct === "true" ;
if (truerep) {
  selectbtn.classList.add ("correct") ;
  score++ ;
} else {
  selectbtn.classList.add ("incorret") ;
}
Array.from (repbutton.children).forEach(btn => {
  let button= btn as HTMLButtonElement;
  if (button.dataset.correct === "true") {
    button.classList.add("correct") ;
  }
  button.disabled = true ;
}) ;
  btnSuivant.style.display = "block" ;
}

function montrerscore () {
  reset () ;
  mesquestions.innerHTML = `ton score est de ${score} sur ${questionsrep.length}` ;
  btnSuivant.innerHTML = "Rejouer" ;
  btnSuivant.style.display = "block" ;
}

function Pbuttonsuivant () {
  indexquestion++ ;
  if (indexquestion<questionsrep.length) {
    MontrerQuestions() ;
  } else {
    montrerscore () ;
  }
}

btnSuivant.addEventListener ( "click" , ()=> {
  if (indexquestion<questionsrep.length) {
    Pbuttonsuivant ()
  } else {
    lancerjeux ()
  }
}) ;

lancerjeux () ;
